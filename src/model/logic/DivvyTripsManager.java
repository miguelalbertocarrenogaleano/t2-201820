package model.logic;

import java.io.FileReader;

import com.opencsv.CSVReader;

import api.IDivvyTripsManager;
import model.vo.VOStation;
import model.vo.VOTrip;
import model.data_structures.DoublyLinkedList;
import model.data_structures.LinkedList;

public class DivvyTripsManager implements IDivvyTripsManager 
{
	//------------------------------------------
	// ATRIBUTOS
	// ----------------------------------------
	
	private LinkedList<VOStation> listStations;
	
	private LinkedList<VOTrip> listTrips;
	
	
	public void loadStations (String stationsFile)
	{
		// TODO Auto-generated method stub
		listStations = new LinkedList<>();
		try 
		{
			CSVReader reader = new CSVReader(new FileReader(stationsFile));
			String [] nextline;
			while ((nextline = reader.readNext())!=null)
			{
				VOStation newStation = new VOStation(nextline[0],nextline[1],nextline[2],nextline[3],nextline[4],nextline[5],nextline[6]);
				listStations.addLast(newStation);
			}
			reader.close();
		}
		catch (Exception e)
		{
			
		}
	}

	
	public void loadTrips (String tripsFile)
	{
		// TODO Auto-generated method stub
		listTrips = new LinkedList<VOTrip>();
		try 
		{
			CSVReader reader = new CSVReader(new FileReader(tripsFile));
			String [] nextline;
			while ((nextline = reader.readNext())!=null)
			{
				VOTrip newTrip = new VOTrip(nextline[0],nextline[1],nextline[2],nextline[3],nextline[4],nextline[5],nextline[6],nextline[7],nextline[8],nextline[9],nextline[10],nextline[11]);
				listTrips.addLast(newTrip);
			}
			reader.close();
		}
		catch (Exception e)
		{
			
		}
	}
	
	
	@Override
	public LinkedList<VOTrip> getTripsOfGender (String gender) 
	{
		// TODO Auto-generated method stub
		LinkedList<VOTrip> lista = new LinkedList<VOTrip>();
		int size = listTrips.size();
		VOTrip actual = null;
		for (int i=0;i < size; i++)
		{
			actual = listTrips.next(i);
			if (actual != null)
			{
				if (!actual.getGender().equalsIgnoreCase("") && actual.getGender().equalsIgnoreCase(gender)) lista.addLast(actual);
			}
		}
		return lista;
	}

	@Override
	public LinkedList <VOTrip> getTripsToStation (int stationID) 
	{
		// TODO Auto-generated method stub
		String newStationId = Integer.toString(stationID);
		LinkedList<VOTrip> lista = new LinkedList<VOTrip>();
		int size = listTrips.size();
		VOTrip actual = null;
		for (int i=0;i < size; i++)
		{
			actual = listTrips.next(i); 
			if (actual.getToStationId().equalsIgnoreCase(newStationId)) lista.addLast(actual);
		}	
		return lista;
	}	
}
