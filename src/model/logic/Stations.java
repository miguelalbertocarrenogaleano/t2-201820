package model.logic;

import java.util.Date;

public class Stations 
{

	//----------------------------
	//ATRIBUTOS
	//---------------------------
	private String trip_id;
	
	private String start_time;
	
	private String end_time;
	
	private String bikeid;
	
	private String tripduration;
	
	private String from_station_id;
	
	private String from_station_name;
	
	private String to_station_id;

	private String to_station_name;
	
	private String usertype;
	
	private String gender;
	
	private String birthyear;
	
	//-------------------------------
	//CONSTRUCTOR
	//-------------------------------
	
	public Stations(String pTrip_id,String pStart_time, String pEnd_time, String pBikeid, String pTripduration, String pFrom_station_id, String pFrom_station_name, String pTo_station_id, String pTo_station_name, String pUsertype, String pGender, String pBirthyear)
	{
		trip_id = pTrip_id;
		
		start_time = pStart_time;
		
		end_time = pEnd_time;
		
		bikeid = pBikeid;
		
		tripduration = pTripduration;
		
		from_station_id = pFrom_station_id;
		
		from_station_name = pFrom_station_name;
		
		to_station_id = pTo_station_id;
		
		to_station_name = pTo_station_name;
		
		usertype = pUsertype;
		
		birthyear = pBirthyear;
	}
	
	

}
