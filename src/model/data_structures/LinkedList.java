package model.data_structures;

import java.util.NoSuchElementException;

/**
 * Esta clase corresponde a la implementaci�n de una lista doblemente encadenada, aunque su nombre sea LinkedList. 
 * @author MIGUEL
 *
 * @param <T> - T es la clase que se va a guardar en los nodos de la lista.
 */
public class LinkedList<T> 
{

	//---------------------------------
	// ATRIBUTOS
	//---------------------------------
	/**
	 * Tama�o de la lista enlazada
	 */
	private int size;
	
	/**
	 * cabeza de la lista
	 */
	private Node<T> head;
	
	/**
	 * Cola de la lista
	 */
	private Node<T> tail;
	
	/**
	 * Nodo actual para el iterador
	 */
	private Node<T> current;
	
	//----------------------------------
	//NESTED CLASS
	//----------------------------------
	
	private static class Node<T>
	{
		/**
		 * Elemento que guarda cada nodo
		 */
		private T element;
	
		/**
		 * Siguiente elemento de la lista.
		 */
		private Node<T> next;
		
		/**
		 * Elemento anterior de la lista.
		 */
		private Node<T> last;
		
		
		/**
		 * Constructor
		 * @param pElemento - elemento a guardar
		 * @param pSiguiente - elemento siguiente a guardar
		 */
		public Node(T pElement, Node<T> pNext, Node<T> pLast)
		{
			element = pElement;
			
			next = pNext;
			
			last = pLast;
		}
		
		/**
		 * @return elemento del nodo
		 */
		public T getElement()
		{
			return element;
		}
		
		/**
		 * @return siguiente nodo
		 */
		public Node<T> getNext()
		{
			return next;
		}

		/**
		 * @return nodo anterior
		 */
		public Node<T> getLast()
		{
			return last;
		}
		
		public void  setNext(Node<T> pNext)
		{
			next = pNext;
		}
		
		public void setLast(Node<T> pLast)
		{
			last = pLast;
		}
	}
	

	//---------------------------------
	//CONSTRUCTOR
	//---------------------------------
	
	public LinkedList()
	{
		size = 0;
		
		head = null;
		
		tail = null;
	}
	
	//----------------------------------
	//M�TODOS
	//----------------------------------
	
	/**
	 * 
	 * @return tama�o de la estructura
	 */
	public int size()
	{
		return size;
	}
	
	/**
	 * Determina si la estructura est� vac�a.
	 * @return 0 si la estructura tiene elementos, 1 de lo contrario.
	 */
	public boolean isEmpty()
	{
		return size==0;
	}
	/**
	 * 
	 * @return primer elemento de la lista
	 */
	public T first()
	{
		if (size==0) return null;
		return head.getElement();
	}
	
	/** 
	 * @return �ltimo elemento de la lista
	 */
	public T last()
	{
		if (size==0) return null;
		return tail.getElement();
	}
	/**
	 * Agrega un nuevo elemento al inicio de la lista
	 * @param pFirst
	 */
	public void addFirst(T pFirst)
	{
		head = new Node<>(pFirst, head, null);
		if (size==0) tail=head;
		size ++;		
	}
	
	/**
	 * Agrega un nuevo elemento al final de la lista
	 * @param pLast
	 */
	public void addLast(T pLast)
	{
		Node<T> nuevo = new Node<>(pLast, null, tail);
		if (size!=0)  
		{
			tail.setNext(nuevo);
			tail = nuevo; 
			size ++;	
		}
		else
		{
			tail = nuevo;
			head = tail;
			size ++;
		}
	}
	
	/**
	 * Retira el primer elemento de la lista
	 * @return primer elemento
	 */
	public T removeFirst()
	{
		if (size==0) return null;
		T aux = head.getElement(); 
		head = head.getNext();
		head.setLast(null);
		size --;
		if (size==0) tail = null;
		return aux;
	}
	
	/**
	 * 
	 * @return boolean - tiene siguiente elemento
	 */
	public boolean hasNext()
	{
		return current.getNext() != null;
	}
	
	public T next(int i) throws NoSuchElementException
	{
		if(i==0) current = head; 
		else if (i < size) current = current.getNext();
		else throw new NoSuchElementException();
		return current.getElement();
	}
		
}