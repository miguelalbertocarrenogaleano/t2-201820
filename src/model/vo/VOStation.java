package model.vo;

public class VOStation 
{
	//----------------------
		// ATRIBUTOS
		// ---------------------
		
		/**
		 * Station id
		 */
		private String id;
		
		/**
		 * Station name
		 */
		private String name;
		/**
		 * Station city
		 */
		private String city;
		/**
		 * Station latitude
		 */
		private String latitude;
		/**
		 * Station longitude
		 */
		private String longitude;
		/**
		 * station parking capacity
		 */
		private String dpcapacity;
		/**
		 * Station online_date
		 */
		private String online_date;
		
		//------------------------------
		// CONSTRUCTOR
		//------------------------------
		
		public VOStation(String pId,String pName,String pCity,String pLatitude,String pLongitude,String pDpcapacity,String pOnlinedate)
		{
			id = pId;
			
			name = pName;
			
			city = pCity;
			
			latitude = pLatitude;
			
			longitude = pLongitude;
			
			dpcapacity = pDpcapacity;
			
			online_date = pOnlinedate;
		}
		
		//------------------------------
		// M�TODOS
		//------------------------------
		/**
		 * 
		 * @return id - Station id.
		 */
		public String getId()
		{
			return id;
		}
		
		/**
		 * 
		 * @return name - station name.
		 */
		public String getName()
		{
			return name;
		}
		
		/**
		 * 
		 * @return city - station city
		 */
		public String getCity()
		{
			return city;
		}
		
		/**
		 * 
		 * @return latitude - Station latitude.
		 */
		public String getLatitude()
		{
			return latitude;
		}
		
		/**
		 * 
		 * @return longitude - station longitude.
		 */
		public String getLongitude()
		{
			return longitude;
		}
		
		/**
		 * 
		 * @return dpcapacity - parking capacity.
		 */
		public String getDpcapacity()
		{
			return dpcapacity;
		}
		
		/**
		 * 
		 * @return online_date - station online date.
		 */
		public String getOnlineDate()
		{
			return online_date;
		}

}
