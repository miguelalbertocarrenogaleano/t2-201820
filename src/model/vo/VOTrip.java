package model.vo;

/**
 * Representation of a Trip object
 */
public class VOTrip 
{

	//----------------------------
		//ATRIBUTOS
		//---------------------------
		private String trip_id;
		
		private String start_time;
		
		private String end_time;
		
		private String bikeid;
		
		private String tripduration;
		
		private String from_station_id;
		
		private String from_station_name;
		
		private String to_station_id;

		private String to_station_name;
		
		private String usertype;
		
		private String gender;
		
		private String birthyear;
		
		//-------------------------------
		//CONSTRUCTOR
		//-------------------------------
		
		public VOTrip(String pTrip_id,String pStart_time, String pEnd_time, String pBikeid, String pTripduration, String pFrom_station_id, String pFrom_station_name, String pTo_station_id, String pTo_station_name, String pUsertype, String pGender, String pBirthyear)
		{
			trip_id = pTrip_id;
			
			start_time = pStart_time;
			
			end_time = pEnd_time;
			
			bikeid = pBikeid;
			
			tripduration = pTripduration;
			
			from_station_id = pFrom_station_id;
			
			from_station_name = pFrom_station_name;
			
			to_station_id = pTo_station_id;
			
			to_station_name = pTo_station_name;
			
			gender = pGender;
			
			usertype = pUsertype;
			
			birthyear = pBirthyear;
		}
		
	
	/**
	 * @return id - Trip_id
	 */
	public String id() 
	
	{
		// TODO Auto-generated method stub
		return trip_id;
	}	
	/**
	 * 
	 * @return start_time - Date when trip started.
	 */
	public String getStartTime ()
	{
		return start_time;
	}
	/**
	 * 
	 * @return end_time - Date when trip finished.
	 */
	public String getEndTime()
	{
		return end_time;
	}
	/**
	 * 
	 * @return bikeid - Id of Bike 
	 */
	public String getBikeId()
	{
		return bikeid;
	}

	
	/**
	 * @return tripduration - Time of the trip in seconds.
	 */
	public String getTripSeconds() {
		// TODO Auto-generated method stub
		return tripduration;
	}
	/**
	 * 
	 * @return from_station_id - ID of starting station. 
	 */
	public String getFromStationId ()
	{
		return from_station_id;
	}
	
	/**
	 * @return station_name - Origin Station Name .
	 */
	public String getFromStation() 
	
	{
		// TODO Auto-generated method stub
		return from_station_name;
	}
	
	/**
	 * 
	 * @return to_station_id - ID of end station.
	 */
	public String getToStationId()
	{
		return to_station_id;
	}

	
	/**
	 * @return station_name - Destination Station Name .
	 */
	public String getToStation() 
	{
		// TODO Auto-generated method stub
		return to_station_name;
	}
	/**
	 * 
	 * @return usertype - Type of user: Subscriber or Customer
	 */
	public String getUserType()
	{
		return usertype;
	}
	
	/**
	 * @return gender - Gender of rider.
	 */
	public String getGender()
	{
		return gender;
	}
	/**
	 * 
	 * @return birthyear - birthyear of rider. 
	 */
	public String birthyear()
	{
		return birthyear;
	}
}
