package taller2.test;

import junit.framework.*;
import model.data_structures.LinkedList;


public class LinkedListTest extends TestCase
{

	//------------------------------------
	// ATRIBUTOS
	//-----------------------------------
		
		/**
		 * Lista de prueba
		 */
		private LinkedList<Integer> listaPrueba;
		
		private LinkedList<String> listaPrueba2;
		
		
   //--------------------------------------------
   // M�TODOS
   //--------------------------------------------
		
		/**
		 * Escenario 1 de prueba 
		 */
		public void setupEscenario1()
		{
			listaPrueba = new LinkedList<Integer>();
		}
		
		/**
		 * Escenario 2 de prueba
		 */
		public void setupEscenario2()
		{
			listaPrueba = new LinkedList<Integer>();
			
			listaPrueba.addFirst(1);
			listaPrueba.addLast(2);
			listaPrueba.addLast(3);
			listaPrueba.addLast(4);
		}
		
		/**
		 * Escenario 3 de prueba
		 */
		public void setupEscenario3()
		{
			listaPrueba2 = new LinkedList<String>();
			
			listaPrueba2.addFirst("Hola");
			listaPrueba2.addLast("buenas");
			listaPrueba2.addLast("que mas");
		}
		
		/**
		 * Prueba del m�todo first()
		 */
		public void testFirst()
		{
			setupEscenario3();
			assertEquals("Las cadenas deben ser iguales","Hola",listaPrueba2.first());
		}
		
		/**
		 * Prueba del m�todo last()
		 */
		public void testLast()
		{
			setupEscenario3();
			assertEquals("Las cadenas deben ser iguales","que mas", listaPrueba2.last());
		}
		
		/**
		 * Prueba del m�todo addFirst()
		 */
		public void testAddFirst()
		{
			setupEscenario1();
			
			listaPrueba.addFirst(1);
			assertTrue("Los n�meros deben ser iguales",1==listaPrueba.first());
			listaPrueba.addFirst(2);
			assertTrue("Los n�meros deben ser iguales",2==listaPrueba.first());						
		}

		/**
		 * Prueba del m�todo addLast();
		 */
		public void testAddLast()
		{
			setupEscenario1();
			listaPrueba.addLast(1);
			assertTrue("Los n�meros deben ser iguales",1==listaPrueba.first());
			listaPrueba.addLast(2);
			assertTrue("Los n�meros deben ser iguales",2==listaPrueba.last());
			
		}
		
		/**
		 * Prueba del m�todo isEmpty()
		 */
		public void testIsEmpty()
		{
			setupEscenario1();
			assertTrue("La afirmaci�n no toma el valor booleano deseado",listaPrueba.isEmpty());			
		}
		
		/**
		 * Prueba del m�todo removeFirst()
		 */
		public void testRemoveFirst() 
		{
			setupEscenario3();
			listaPrueba2.removeFirst();
			assertEquals("Las cadenas deben ser iguales", "buenas", listaPrueba2.first());
		}
		
		/**
		 * Prueba del m�todo size()
		 */
		public void testSize()
		{
			setupEscenario1();
			assertTrue("Los n�meros deben ser iguales",0==listaPrueba.size());
			setupEscenario3();
			assertTrue("Los n�meros deben ser iguales",3==listaPrueba2.size());
		}
}
